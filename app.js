'use strict';

const Hapi = require('hapi');
const mongojs = require('mongojs');
var corsHeaders = require('hapi-cors-headers')

// Create a server with a host and port
const server = new Hapi.Server();
server.connection({
    port: 5554
});

//Connect to db
server.app.db = mongojs('hapi-rest-mongo', ['notebooks','notes']);

server.ext('onPreResponse', corsHeaders);

//Load plugins and start server
server.register([
  require('inert'),
  require('./routes/notebooks'),
  require('./routes/notes')
], (err) => {

  if (err) {
    throw err;
  }

  // Start the server
  server.start((err) => {
    console.log('Server running at:', server.info.uri);
  });

});
