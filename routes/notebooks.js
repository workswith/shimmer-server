'use strict';

const Boom = require('boom');
const shortid = require('shortid');
const Joi = require('joi');

exports.register = function(server, options, next) {

  const db = server.app.db;

  server.route({
    method: 'GET',
    path: '/rest/notebooks',
    handler: function (request, reply) {
      db.notebooks.find((err, docs) => {

        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        reply(docs);
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/rest/notebooks/{id}',
    handler: function (request, reply) {

      db.notebooks.findOne({
          _id: request.params.id
      }, (err, doc) => {

        if (err) {
            return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        if (!doc) {
            return reply(Boom.notFound());
        }

        reply(doc);
      });
    }
  });


  server.route({
    method: 'POST',
    path: '/rest/notebooks',
    handler: function (request, reply) {

      var notebook = request.payload;

      //Create an id (also acts as URL)
      notebook._id = shortid.generate();

      db.notebooks.save(notebook, (err, result) => {

        if (err) {
            return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        reply(notebook);
      });
    }
  });

  server.route({
    method: 'PUT',
    path: '/rest/notebooks/{id}',
    handler: function (request, reply) {

        db.notebooks.update({
          _id: request.params.id
        }, {
          $set: request.payload
        }, function (err, result) {

          if (err) {
            return reply(Boom.wrap(err, 'Internal MongoDB error'));
          }

          if (result.n === 0) {
            return reply(Boom.notFound());
          }

          reply().code(204);
        });
    }
  });

  server.route({
    method: 'DELETE',
    path: '/rest/notebooks/{id}',
    handler: function (request, reply) {

      db.notebooks.remove({
        _id: request.params.id
      }, function (err, result) {

        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        if (result.n === 0) {
          return reply(Boom.notFound());
        }

        reply().code(204);
      });
    }
  });

  return next();
};

exports.register.attributes = {
  name: 'routes-notebooks'
};
