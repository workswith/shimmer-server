'use strict';

const path = require('path');

exports.register = function(server, options, next) {
  // 
  // server.route({
  //     method: 'GET',
  //     path: '/mergedPdfs/{param*}',
  //     handler: {
  //         directory: {
  //             path: 'mergedPdfs',
  //             listing: true
  //         }
  //     }
  // });
  //
  // server.route({
  //     method: 'GET',
  //     path: '/{param*}',
  //     handler: {
  //         directory: {
  //             path: 'dev',
  //             listing: true
  //         }
  //     }
  // });

  //
  // server.route({
  //     method: 'GET',
  //     path: '/stack/{param*}',
  //     handler: function (request, reply) {
  //         reply.file('dev/index.html');
  //     }
  // });
  // server.route({
  //     method: 'GET',
  //     path: '/categories/{param*}',
  //     handler: function (request, reply) {
  //         reply.file('dev/index.html');
  //     }
  // });
  // server.route({
  //     method: 'GET',
  //     path: '/page/{param*}',
  //     handler: function (request, reply) {
  //         reply.file('dev/index.html');
  //     }
  // });
  // server.route({
  //     method: 'GET',
  //     path: '/resources/{param*}',
  //     handler: function (request, reply) {
  //         reply.file('dev/index.html');
  //     }
  // });

  return next();
};

exports.register.attributes = {
  name: 'routes-main'
};
