'use strict';

const Boom = require('boom');
const shortid = require('shortid');
const Joi = require('joi');

exports.register = function(server, options, next) {

  const db = server.app.db;

  server.route({
    method: 'GET',
    path: '/rest/notes',
    handler: function (request, reply) {
      db.notes.find((err, docs) => {

        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        reply(docs);
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/rest/notes/{id}',
    handler: function (request, reply) {

      db.notes.findOne({
          _id: request.params.id
      }, (err, doc) => {

        if (err) {
            return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        if (!doc) {
            return reply(Boom.notFound());
        }

        reply(doc);
      });
    }
  });


  server.route({
    method: 'POST',
    path: '/rest/notes',
    handler: function (request, reply) {

      var note = request.payload;

      //Create an id (also acts as URL)
      note._id = shortid.generate();

      db.notes.save(note, (err, result) => {

        if (err) {
            return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        reply(note);
      });
    }
  });

  server.route({
    method: 'PUT',
    path: '/rest/notes/{id}',
    handler: function (request, reply) {

        db.notes.update({
          _id: request.params.id
        }, {
          $set: request.payload
        }, function (err, result) {

          if (err) {
            return reply(Boom.wrap(err, 'Internal MongoDB error'));
          }

          if (result.n === 0) {
            return reply(Boom.notFound());
          }

          reply().code(204);
        });
    }
  });

  server.route({
    method: 'DELETE',
    path: '/rest/notes/{id}',
    handler: function (request, reply) {

      db.notes.remove({
        _id: request.params.id
      }, function (err, result) {

        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        if (result.n === 0) {
          return reply(Boom.notFound());
        }

        reply().code(204);
      });
    }
  });

  return next();
};

exports.register.attributes = {
  name: 'routes-notes'
};
