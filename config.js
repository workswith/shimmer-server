'use strict';

// Feel free to extend this interface
// depending on your app specific config.
exports.register = {
  'Status': 'DEV',
  'DEV': {
    'baseUrl': 'http://localhost:5555',
    'nodeUrl': 'http://localhost:5554'
  },
  'PROD': {
    'baseUrl': '',
    'nodeUrl': ''
  }
}
