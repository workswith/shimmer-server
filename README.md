# API Server Functionality

MongoDB must be installed.

$npm install

Then start MongoDB:
$/Applications/mongodb/bin/mongod --dbpath=./data --port 27017

Then start the app...

$node app

Now you should get a localhost address for the API

# Full App Installation

If you want to build and serve the full app, you must do the following steps:
1. In the `web-app` repo, run: `npm run build.dev` OR `npm run build.prod`
2. Copy the created folder, either dist/dev or dist/prod to the root directory of the `api-server` repo
3. Enter the dev or prod folder and do an `npm install`. You will get typing errors - that's fine.
4. Look in the routes/main.js and fix the routing to the right URL
5. Run a Wordpress Server that has WP-API, ACF, and ACF to WP-API installed.
6. Run a MongoDB server using /data as the database path
7. Run `forever start app.js`

And you have a fancy app running.
